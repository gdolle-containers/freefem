#!/bin/sh
set -eu

# Image informations.
ffversion=4.10
name="freefem"
tag=${ffversion}-ubuntu-deb
author="Guillaume_Dollé"
dist=ubuntu
dist_tag=20.04
#entrypoint='["/usr/local/bin/FreeFem++-mpi"]'

pkg_list="
build-essential
libgsl-dev 
libhdf5-dev
liblapack-dev 
libopenmpi-dev 
freeglut3-dev
curl
libgfortran4
"

########################################
# BUILDAH HELPER FUNCTIONS.
########################################

# Print custom messages with colors.
MSG() 
{
    bold=$(tput bold)
    red=$(tput setaf 1)
    green=$(tput setaf 2)
    yellow=$(tput setaf 3)
    blue=$(tput setaf 4)
    cyan=$(tput setaf 6)
    reset=$(tput sgr0)
    case $1 in
        "STEP")  printf "${bold}${green}[$2]${reset}\n" ;;
        "INFO")  printf "${bold}${cyan}$2${reset}\n" ;;
        "WARN")  printf "${bold}${yellow}$2${reset}\n" ;;
        "ERROR") printf "${bold}${red}$2${reset}\n" ;;
    esac
}
BASE(){ export cont=`buildah from $1`; }
RUN(){  buildah run ${cont} -- "$@"; }
CLEAN(){ buildah rm ${cont}; }
CONF(){ buildah config $@ ${cont}; }
COMMIT(){ buildah commit ${cont} $@; }

TZ=Europe/Paris

########################################
# MAIN RECIPE.
########################################

# Avoid remaining unfinished containers.
trap_exit(){
MSG     "STEP" "Remove container..."
CLEAN
}
trap trap_exit EXIT


BASE    ${dist}:${dist_tag}

MSG     "STEP" "Updating ${dist} packages..."
RUN     ln -snf /usr/share/zoneinfo/$TZ /etc/localtime
RUN     echo $TZ \> /etc/timezone
RUN     apt-get -y update
RUN     apt-get -y autoremove

MSG     "INFO" "Enable universe repository..."
RUN     apt-get -y install software-properties-common
RUN     add-apt-repository universe

MSG     "STEP" "Installing apt packages..."
RUN     apt-get -y install ${pkg_list}

MSG     "STEP" "Installing freefem deb packages..."
RUN     curl -o freefem.deb -L "https://github.com/FreeFem/FreeFem-sources/releases/download/v4.9/FreeFEM_4.9_Ubuntu_withPETSc_amd64.deb"
RUN     dpkg -i freefem.deb
RUN     apt-get install -f

MSG     "STEP" "Fix HDF5 missing library with symlink"
RUN     ln -s /usr/lib/x86_64-linux-gnu/libhdf5_serial.so /usr/lib/x86_64-linux-gnu/libhdf5_serial.so.100
RUN     ln -s /usr/lib/x86_64-linux-gnu/libmpi.so /usr/lib/x86_64-linux-gnu/libmpi.so.20
RUN     ln -s /usr/lib/x86_64-linux-gnu/libmpi_cxx.so /usr/lib/x86_64-linux-gnu/libmpi_cxx.so.20

MSG     "STEP" "Cleaning container..."
RUN     apt-get -y autoremove && apt-get -y clean --dry-run && apt-get -y purge
RUN     rm -rf /var/cache/apt
RUN     rm -rf /${name}
RUN     rm -rf freefem.deb

MSG     "STEP" "Configuring container..."
CONF    --author ${author} --created-by ${author} --label name=${name}
#CONF    --entrypoint ${entrypoint}

MSG     "STEP" "Create OSI image..."
COMMIT  ${name}:${tag}
