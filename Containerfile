FROM    ubuntu:22.04 as base

ENV     FF_VERSION=4.14
ENV     TZ=Europe/Paris
ENV     LANG=en_US.UTF-8

LABEL   version=${FF_VERSION}
LABEL   description="A PDE oriented language using the finite element method"

RUN     ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone
RUN     apt update -y && apt -y install software-properties-common \
                   && add-apt-repository -y universe \
                   && apt -y install \
                        build-essential \
                        libgsl-dev \
                        libhdf5-dev \
                        liblapack-dev \
                        libopenmpi-dev \
                        freeglut3-dev \
                        curl \
                        libgfortran5 \
                        bison \
                        flex \
                        m4 \
                        unzip \
                        patch \
                        wget \
                        git \
                        python3 \
                        python3-distutils \
                    && rm -rf /var/lib/apt/lists/*


FROM base as ff-src

RUN     curl -o freefem.tar.gz -L "https://github.com/FreeFem/FreeFem-sources/archive/refs/tags/v${FF_VERSION}.tar.gz";\
        tar -xzvf freefem.tar.gz; \
        rm -rf freefem.tar.gz; \
        cd /FreeFem-sources-${FF_VERSION} \
                && autoreconf -i \
                && ./configure --enable-download --enable-optim --prefix=/usr \
                && ./3rdparty/getall -a; \
        cd /FreeFem-sources-${FF_VERSION}/3rdparty/ff-petsc \
            && make petsc-slepc; \
        cd /FreeFem-sources-${FF_VERSION} \
            && ./reconfigure \
            && make ${MAKEFLAGS} \
            && make check \
            && make install; \
        rm -rf /FreeFem-sources-${FF_VERSION}
