#!/bin/sh
set -eu

# Image informations.
ffversion=4.13
name="freefem"
tag=${ffversion}-src-ubuntu
author="Guillaume_Dollé"
dist=ubuntu
dist_tag=22.04
#entrypoint='["/usr/bin/FreeFem++-mpi"]'
MAKEFLAGS="-j10"

pkg_list="
build-essential
libgsl-dev 
libhdf5-dev
liblapack-dev 
libopenmpi-dev 
freeglut3-dev
curl
libgfortran5
bison
flex
m4
unzip
patch
wget
git
python3
python3-distutils
"

########################################
# BUILDAH HELPER FUNCTIONS.
########################################

# Print custom messages with colors.
MSG() 
{
    bold=$(tput bold)
    red=$(tput setaf 1)
    green=$(tput setaf 2)
    yellow=$(tput setaf 3)
    blue=$(tput setaf 4)
    cyan=$(tput setaf 6)
    reset=$(tput sgr0)
    case $1 in
        "STEP")  printf "${bold}${green}[$2]${reset}\n" ;;
        "INFO")  printf "${bold}${cyan}$2${reset}\n" ;;
        "WARN")  printf "${bold}${yellow}$2${reset}\n" ;;
        "ERROR") printf "${bold}${red}$2${reset}\n" ;;
    esac
}
BASE(){ export cont=`buildah from $1`; }
RUN(){  buildah run ${cont} -- "$@"; }
CLEAN(){ buildah rm ${cont}; }
CONF(){ buildah config $@ ${cont}; }
COMMIT(){ buildah commit ${cont} $@; }

TZ=Europe/Paris

########################################
# MAIN RECIPE.
########################################

# Avoid remaining unfinished containers.
trap_exit(){
MSG     "STEP" "Remove container..."
CLEAN
}
trap trap_exit EXIT


BASE    ${dist}:${dist_tag}

MSG     "STEP" "Updating ${dist} packages..."
RUN     ln -snf /usr/share/zoneinfo/$TZ /etc/localtime
RUN     echo $TZ \> /etc/timezone
RUN     apt -y update

MSG     "INFO" "Enable universe repository..."
RUN     apt -y install software-properties-common
RUN     add-apt-repository -y universe

MSG     "STEP" "Installing apt packages..."
RUN     apt -y install ${pkg_list}

MSG     "STEP" "Compiling from source..."
RUN     curl -o freefem.tar.gz -L "https://github.com/FreeFem/FreeFem-sources/archive/refs/tags/v${ffversion}.tar.gz"
RUN     tar -xzvf freefem.tar.gz
RUN     bash -c "cd /FreeFem-sources-${ffversion} &&
                autoreconf -i &&
                ./configure --enable-download --enable-optim --prefix=/usr &&
                ./3rdparty/getall -a"
RUN     bash -c "cd FreeFem-sources-${ffversion}/3rdparty/ff-petsc && make petsc-slepc"
RUN     bash -c "cd FreeFem-sources-${ffversion} && ./reconfigure && make ${MAKEFLAGS} && make check && make install"

MSG     "STEP" "Cleaning container..."
RUN     apt -y autoremove 
RUN     apt -y clean --dry-run 
RUN     apt -y purge
RUN     rm -rf /var/cache/apt
RUN     rm -rf /freefem.tar.gz
RUN     rm -rf /FreeFem-sources-${ffversion}

MSG     "STEP" "Configuring container..."
CONF    --author ${author} --created-by ${author} --label name=${name}
#CONF    --entrypoint ${entrypoint}

MSG     "STEP" "Create OSI image..."
COMMIT  ${name}:${tag}
